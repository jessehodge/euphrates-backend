# Euphrates

Euphrates is the exciting new universal app that allows you to demand any service (e.g. jobs, projects, tasks) anytime, anywhere. Conversely, you can promote and offer your expertise. Whether you are in need of assistance, or one who can provide it, Euphrates is the real-time, highly flexible, and user-friendly application that's perfect for you.

## Integrations

The Euphrates backend is integrated with the following external libraries that may require new keys for you to use:

1. Firebase ~ For real time chat communication
2. Sinch ~ For real time VOIP communication

No personal data is used in the making of the calls or chat, simply uses the Devices ID and the user's id that is made when they register on the app.

## Installation

1. Git clone this repository

```python
git clone git@bitbucket.org:jessehodge/euphrates-backend.git
```

2. Install virtualenv (if you don't have it already) here is the command to install.

```python
python3 -m pip install virtualenv
```

3. Create a python3 virtualenv

```python
python3 -m venv env
```

4. Source the virtualenv to be able to install all necessary dependencies without conflicts to your system

```python
source env/bin/activate
```

5. Use the package manager [pip](https://pip.pypa.io/en/stable/) to install all dependencies from requirements.txt

```bash
pip install -r requirements.txt
```

6. Run django's makemigrations to guarantee all necessary migration changes were made.

```python
./manage.py makemigrations
```

7. Run django's migrate to create the database and then proceed to run the app itself.

```python
./manage.py migrate
./manage.py runserver
```

##Stage

Currently in the MVP stage.
-- Authentication
-- Search for Provider within 50 miles
-- Create Job Tags
-- Place in-screen call
-- Live chat
-- Payment of Escrow
-- Creation of invoice
-- Payment of Invoices

```bash
After installation API Docs at http://localhost:8000/api/docs/ 
```
![Screenshot](euphrates_backend_image.png)