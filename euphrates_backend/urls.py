"""euphrates_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny

from v1 import urls as v1_urls

schema_view = get_schema_view(
    openapi.Info(
        title='Euphrates API',
        default_version='v1',
        description='Test Description',
    ),
    public=True,
    permission_classes=(AllowAny,)
)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/docs/', schema_view.with_ui('redoc', cache_timeout=0)),
    url(r'^api/v1/', include(v1_urls)),
]