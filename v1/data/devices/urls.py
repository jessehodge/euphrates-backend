from rest_framework_bulk.routes import BulkRouter

from .views import DeviceViewSet

router = BulkRouter()

router.register('devices', DeviceViewSet)

urlpatterns = []

urlpatterns += router.urls
