from fcm_django.api.rest_framework import FCMDeviceViewSet
from rest_framework.permissions import AllowAny

from v1.data.devices.models import Devices
from v1.data.devices.serializers import DeviceSerializer


class DeviceViewSet(FCMDeviceViewSet):
    queryset = Devices.objects.all()
    serializer_class = DeviceSerializer
    permission_classes = (AllowAny,)
