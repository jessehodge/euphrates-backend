from django.db import models
from fcm_django.models import AbstractFCMDevice


class Devices(AbstractFCMDevice):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
