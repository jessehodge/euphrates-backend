from fcm_django.api.rest_framework import FCMDeviceSerializer

from v1.data.devices.models import Devices


class DeviceSerializer(FCMDeviceSerializer):
    class Meta:
        model = Devices
        fields = '__all__'
