from v1.data.devices.models import Devices
from v1.data.jobs.models import Jobs
from v1.data.users.models import User

from celery import Celery, task


celery = Celery('tasks', broker='amqp://guest@localhost//')

def job_notification_to_consumer(instance_id):
    job = Jobs.objects.get(pk=instance_id)
    for consumer in job.consumers.all():
        for provider in job.providers.all():
            devices = Devices.objects.filter(user__email=consumer)
            provider_id = User.objects.get(email=provider)
            for dev in devices:
                dev.send_message(
                    data= {
                        "method": "",
                        "room_id": job.chat.room_id,
                        "job_id": instance_id,
                        "provider_id": provider_id.id,
                        "consumer_name": provider_id.name,
                        "job_base_rate": str(job.job_base_rate),
                        "job_unit_rate": str(job.job_unit_rate),
                    },
                         title="Hello", body="this is a test",
                         click_action="OPEN_PROVIDER_ACCEPTANCE_ACTIVITY"
                )

@task()
def unit_rate_edit_task(instance, request):
    email = str(request.data["user_email"])
    if str(instance.consumers.all().first()) == email:
        provider_devices = Devices.objects.filter(user__email=instance.providers.all().first())
        unit_rate_message(request, provider_devices)
    elif str(instance.providers.all().first()) == email:
        consumer_devices = Devices.objects.filter(user__email=instance.consumers.all().first())
        unit_rate_message(request, consumer_devices)

@task()
def base_rate_edit_task(instance, request):
    if str(instance.consumers.all().first()) == str(request.data["user_email"]):
        provider_devices = Devices.objects.filter(user__email=instance.providers.all().first())
        base_rate_message(request, provider_devices)
    elif str(instance.providers.all().first()) == str(request.data['user_email']):
        consumer_devices = Devices.objects.filter(user__email=instance.consumers.all().first())
        base_rate_message(request, consumer_devices)

@task()
def unit_rate_message(request, device_array):
    for dev in device_array:
        dev.send_message(data={"method": "unit_rate_changed",
                               "job_unit_rate": request.data['job_unit_rate'],
                               "job_id": request.data['job_id'],
                               "consumer_id": request.data['consumer_id'],
                               "provider_id": request.data['provider_id']})

@task()
def base_rate_message(request, device_array):
    for dev in device_array:
        dev.send_message(data={"method": "base_rate_changed",
                               "job_base_rate": request.data['job_base_rate'],
                               "job_id": request.data['job_id'],
                               "consumer_id": request.data['consumer_id'],
                               "provider_id": request.data['provider_id']})
