from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.viewsets import ModelViewSet

from v1.data.devices.models import Devices
from v1.data.job_tags.models import JobTags
from v1.data.jobs.models import Jobs
from v1.data.users.models import User
from v1.data.jobs.serializers import JobsSerializer, ConsumerJobSerializer
from v1.data.jobs.tasks import unit_rate_edit_task, base_rate_edit_task, job_notification_to_consumer


class BaseRateViewSet(ModelViewSet):
    queryset = Jobs.objects.all()
    serializer_class = JobsSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        base_rate_edit_task(instance, request)
        return Response(serializer.data)


class UnitRateViewSet(ModelViewSet):
    queryset = Jobs.objects.all()
    serializer_class = JobsSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        unit_rate_edit_task(instance, request)
        return Response(serializer.data)


class JobsViewSet(ModelViewSet):
    queryset = Jobs.objects.all()
    serializer_class = JobsSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def update(self, request, *args, **kwargs):
        # Get provider's job_tag info and update the job
        provs = JobTags.objects.get(providers=request.data["providers"])
        instance = self.get_object()
        job_serialzier = self.get_serializer(
            instance,
            data={
                "providers": [request.data['providers']]
            }, partial=True)
        if job_serialzier.is_valid(raise_exception=True):
            job_serialzier.save()
        serializer = self.get_serializer(instance,
            data= {
                "job_base_rate": provs.job_base_rate,
                "job_unit_rate": provs.job_unit_rate,
            },
                partial=True
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        job_notification_to_consumer(instance.id)
        return Response(serializer.data, HTTP_200_OK)


class ProviderFinalInvoice(ModelViewSet):
    queryset = Jobs.objects.all()
    serializer_class = JobsSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        for consumer in instance.consumers.all():
            device = Devices.objects.get(user_id=consumer)
            device.send_message(
                title="Euphrates Invoice",
                body="You have an invoice waiting for you!",
                data={
                    "method": "final_invoice",
                    "job_unit_rate": serializer.data['job_unit_rate'],
                    "number_of_units": serializer.data['number_of_units']
                },
                click_action="OPEN_CONSUMER_FINAL_INVOICE"
            )
        return Response(serializer.data, HTTP_200_OK)


class ConsumerJobsViewSet(ListAPIView):
    serializer_class = ConsumerJobSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        consumer_id = self.kwargs['consumer_id']
        return Jobs.objects.filter(consumers=consumer_id)
