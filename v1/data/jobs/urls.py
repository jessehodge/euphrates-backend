from django.urls import re_path
from rest_framework_bulk.routes import BulkRouter

from v1.data.jobs.views import JobsViewSet, BaseRateViewSet, UnitRateViewSet, ProviderFinalInvoice, ConsumerJobsViewSet

router = BulkRouter()
router.register('job', JobsViewSet)
router.register('provider_job_update', ProviderFinalInvoice)
router.register('base_rate', BaseRateViewSet)
router.register('unit_rate', UnitRateViewSet)

urlpatterns = [
    re_path('^create_job/$', JobsViewSet.as_view({'post': 'create'})),
    re_path('^consumer_jobs/(?P<consumer_id>.+)$', ConsumerJobsViewSet.as_view())
]

urlpatterns += router.urls
