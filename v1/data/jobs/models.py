from django.db import models

from v1.data.chats.models import Chats
from v1.data.job_tags.models import JobTags
from v1.data.users.models import User


# Create your models here.


class Jobs(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="owner", null=True)
    chat = models.ForeignKey(Chats, on_delete=models.CASCADE, related_name="chat", null=True)
    job_tag = models.ForeignKey(JobTags, on_delete=models.CASCADE, related_name="job_tag", null=True)
    providers = models.ManyToManyField(User, related_name="providers")
    consumers = models.ManyToManyField(User, related_name="consumers")
    job_base_rate = models.DecimalField(
        decimal_places=2, max_digits=10, blank=True, null=True,
        help_text='Amount we take to escrow before a job begins'
    )
    job_unit_rate = models.DecimalField(
        decimal_places=2, max_digits=10, blank=True, null=True, help_text='Rate per unit'
    )
    unit_total_charged = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    number_of_units = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
    unit = models.CharField(max_length=50, blank=True, null=True)  # Name of the unit
    status = models.CharField(max_length=40, null=True, blank=True)  # Status of job
