from rest_framework.serializers import ModelSerializer

from v1.data.jobs.models import Jobs
from v1.data.users.serializers import UserSerializer


class JobsSerializer(ModelSerializer):

    class Meta:
        model = Jobs
        fields = '__all__'


class ConsumerJobSerializer(ModelSerializer):
    consumers = UserSerializer(many=True, read_only=True)

    class Meta:
        model = Jobs
        fields = '__all__'
