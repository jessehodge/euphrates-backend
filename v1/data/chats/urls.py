from rest_framework_bulk.routes import BulkRouter

from v1.data.chats.views import ChatViewSet

router = BulkRouter()

router.register('chat', ChatViewSet)

urlpatterns = []

urlpatterns += router.urls
