from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from v1.data.chats.models import Chats
from v1.data.chats.serializers import ChatSerializer


class ChatViewSet(ModelViewSet):
    queryset = Chats.objects.all()
    serializer_class = ChatSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()
