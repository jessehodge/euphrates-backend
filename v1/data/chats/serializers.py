from rest_framework.serializers import ModelSerializer

from v1.data.chats.models import Chats


class ChatSerializer(ModelSerializer):
    class Meta:
        model = Chats
        fields = '__all__'
