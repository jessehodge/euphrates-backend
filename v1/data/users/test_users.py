from django.urls import reverse
from rest_framework.test import APITestCase

from v1.data.users.user_factory import UserFactory


class UserTestCase(APITestCase):

    def setUp(self):
        self.user = UserFactory()
        self.user.save()
        self.login_url = '/api/v1/auth/login/'

    def assert_400_response(self, status):
        self.assertEqual(status, 400)

    def test_user_registration_success(self):
        data = {
            'email': 'jessehodge@gig-antic.com',
            'password': 'password123',
            'name': 'Jesse Hodge',
        }
        response = self.client.post(reverse('register-list'), data)
        self.assertEqual(response.json()['email'], data['email'])
        self.assertEqual(response.json()['name'], data['name'])
        self.assertEqual(response.status_code, 201)

    def test_registration_failure(self):
        data = {
            'email': self.user.email,
            'password': self.user.password,
            'name': self.user.name
        }
        response = self.client.post(reverse('register-list'), data)
        self.assertEqual(response.json()['email'], ['user with this email already exists.'])
        self.assert_400_response(response.status_code)

    def test_user_login_success(self):
        data = {
            'email': self.user.email,
            'password': self.user.password
        }
        response = self.client.post(self.login_url, data)
        self.assertEqual(response.json()['id'], 1)
        self.assertEqual(response.json()['email'], data['email'])
        self.assertEqual(response.status_code, 200)

    def test_user_login_failure_email_not_exist(self):
        data = {
            'email': 'testemail@gig-antic.com',
            'password': 'watpassword'
        }
        response = self.client.post(self.login_url, data)
        self.assertEqual(response.json(), {'message': 'User does not exist'})
        self.assert_400_response(response.status_code)

    def test_user_login_failure_no_password(self):
        data = {
            'email': self.user.email
        }
        response = self.client.post(self.login_url, data)
        self.assertEqual(response.json(), {'password': ['This field is required.']})
        self.assert_400_response(response.status_code)

    def test_user_login_failure_no_email(self):
        data = {
            'password': self.user.password
        }
        response = self.client.post(self.login_url, data)
        self.assertEqual(response.json(), {'email': ['This field is required.']})
        self.assert_400_response(response.status_code)

    def test_user_login_no_fields(self):
        response = self.client.post(self.login_url)
        self.assertEqual(
            response.json(), {'email': ['This field is required.'], 'password': ['This field is required.']}
        )
        self.assert_400_response(response.status_code)