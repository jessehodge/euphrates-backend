import factory

from v1.data.users.models import User


class UserFactory(factory.Factory):
    class Meta:
        model = User

    name = factory.Sequence(lambda x: f"Jesse {x} Customer")
    email = factory.Sequence(lambda x: f"user{x}@gig-antic.com")
    password = "password123"