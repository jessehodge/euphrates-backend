from django.contrib.auth import authenticate
from django.core.validators import EmailValidator
from rest_framework import serializers
from rest_framework.serializers import ValidationError

from v1.data.users.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'email',
            'password',
            'name',
            'longitude',
            'latitude'
        )
        read_only_field = ['id']
        extra_kwargs = {
            'password': {'write_only': True},
            'is_active': {'write_only': True}
        }

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class AuthCustomTokenSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            if EmailValidator(email):
                authenticate(email=email, password=password)
            else:
                raise ValidationError('Unable to log in with provided credentials.')
        return attrs
