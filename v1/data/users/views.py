import logging

from rest_framework import parsers, renderers, status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework_bulk import BulkModelViewSet

from v1.data.users.models import User
from v1.data.users.serializers import UserSerializer, AuthCustomTokenSerializer

logger = logging.getLogger(__name__)


class RegisterView(BulkModelViewSet):
    """
    Register View
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()


class ObtainAuthToken(APIView):
    """
    Login endpoint
    """
    permission_classes = (AllowAny,)
    parser_classes = (
        parsers.FormParser,
        parsers.MultiPartParser,
        parsers.JSONParser,
    )
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthCustomTokenSerializer
    authentication_classes = ()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user = User.objects.get(email=serializer.validated_data['email'])
            if user.longitude is not None:
                user.longitude = request.data['longitude']
            if user.latitude is not None:
                user.latitude = request.data['latitude']
            user.save()
            token, created = Token.objects.get_or_create(user=user)
            logger.info({'message': 'Created a token for a user'})
            return Response({
                'token': token.key,
                'email': user.email,
                'id': user.id,
            })
        except User.DoesNotExist:
            logger.error({'message': 'User does not exist'})
            return Response({'message': 'User does not exist'}, status=status.HTTP_400_BAD_REQUEST)
