from __future__ import unicode_literals

from django.contrib.auth.models import AbstractBaseUser, UserManager
from django.db import models


class User(AbstractBaseUser):
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=200)
    name = models.CharField(max_length=30)
    longitude = models.CharField(max_length=30, null=True, blank=True)
    latitude = models.CharField(max_length=30, null=True, blank=True)

    REQUIRED_FIELDS = ['password']
    USERNAME_FIELD = 'email'

    objects = UserManager()
