from django.urls import re_path
from rest_framework_bulk.routes import BulkRouter

from v1.data.users.views import RegisterView, ObtainAuthToken, UserViewSet

router = BulkRouter()
router.register('auth/user', UserViewSet)
router.register('auth/register', RegisterView, basename='register')

urlpatterns = [
    re_path(r'^auth/login/', ObtainAuthToken.as_view())
]

urlpatterns += router.urls
