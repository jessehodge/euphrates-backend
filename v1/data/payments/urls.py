from django.urls import re_path

from v1.data.payments.views import escrow_payment, final_invoice_payment

urlpatterns = [
    re_path('^escrow_payment/$', escrow_payment),
    re_path('^final_invoice_payment/$', final_invoice_payment)
]
