import stripe
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response


# Create your views here.


@api_view(['POST'])
@permission_classes([])
def escrow_payment(request):
    charge = int(float(request.data['charge']) * 100)
    stripe.Charge.create(
        api_key="sk_test_wJrJl1XIANZvmFPmHmlKuzMp",
        amount=charge,
        source=request.data['source'],
        currency="usd")
    return Response(data={})


@api_view(['POST'])
@permission_classes([])
def final_invoice_payment(request):
    charge = int(float(request.data['charge']) * 100)
    stripe.Charge.create(
        api_key="sk_test_wJrJl1XIANZvmFPmHmlKuzMp",
        amount=charge,
        source=request.data['source'],
        currency="usd")
    return Response(data={})
