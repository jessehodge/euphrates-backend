from v1.data.jobs.models import Jobs
from v1.data.users.models import User

from celery import task

@task
def job_search_task(devices, user, name, consumer_id, room_id, job):
    for dev in devices:
        provider = User.objects.get(devices__device_id=dev)
        job_created_providers = list(Jobs.objects.filter(pk=job.pk).values_list('providers', flat=True))
        if job_created_providers[0] is None and provider:
            # origin_location = (int(float(user.latitude)), float(user.longitude))
            # provider_location = (int(float(provider.latitude)), float(provider.longitude))
            # distance_result = geodesic(origin_location, provider_location).miles
            # if distance_result < 50:
            dev.send_message(data={
                "method": "to_provider",
                "consumer_id": consumer_id,
                "consumer_name": user.name,
                "room_id": room_id,
                "job_id": job.pk,
                "name": name,
            }, title="Hello", body="this is a test", click_action="OPEN_PROVIDER_ACCEPTANCE_ACTIVITY")
            # time.sleep(30)
        else:
            break
