from django.urls import re_path
from rest_framework_bulk.routes import BulkRouter

from v1.data.job_tags.views import JobTagView, JobTagSearchView

router = BulkRouter()

router.register('job_tag', JobTagView)

urlpatterns = [
    re_path('^search/(?P<name>.+)/(?P<consumer_id>.+)/(?P<room_id>.+)$', JobTagSearchView.as_view())
]

urlpatterns += router.urls
