from django.apps import AppConfig


class JobTagsConfig(AppConfig):
    name = 'job_tags'
