from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from v1.data.chats.models import Chats
from v1.data.devices.models import Devices
from v1.data.devices.serializers import DeviceSerializer
from v1.data.job_tags.models import JobTags
from v1.data.job_tags.serializers import JobTagSerializer
from v1.data.job_tags.tasks import job_search_task
from v1.data.jobs.models import Jobs
from v1.data.users.models import User


# Create your views here.


class JobTagView(ModelViewSet):
    queryset = JobTags.objects.all()
    serializer_class = JobTagSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def create(self, request, *args, **kwargs):
        request.data["providers"] = [request.data["providers"]]
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class JobTagSearchView(ListAPIView):
    serializer_class = DeviceSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get_queryset(self):
        name, consumer_id, room_id = self.kwargs['name'], self.kwargs['consumer_id'], self.kwargs['room_id']
        user = User.objects.get(pk=consumer_id)
        chat = Chats.objects.create(room_id=room_id)
        chat.save()
        job = Jobs.objects.create(owner=user, chat=chat)
        job.save()
        job.consumers.add(user.id)
        devices = Devices.objects.filter(user__jobtags__name__contains=name)
        job_search_task(devices, user, name, consumer_id, room_id, job)
        return devices
