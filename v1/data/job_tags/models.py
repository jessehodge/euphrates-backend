from django.db import models

from v1.data.users.models import User


# Create your models here.
class JobTags(models.Model):
    STATUS_CHOICES = (
        ('active', 'Active'),
        ('inactive', 'Inactive'),
    )
    name = models.CharField(max_length=40, blank=True, null=True)  # Name of the tag
    job_base_rate = models.DecimalField(
        decimal_places=2, max_digits=10, blank=True, null=True,
        help_text='Amount we take to escrow before a job begins'
    )
    job_unit_rate = models.DecimalField(
        decimal_places=2, max_digits=10, blank=True, null=True, help_text='Rate per unit'
    )
    unit = models.CharField(max_length=50, blank=True, null=True)  # Name of the unit
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default="Active")
    providers = models.ManyToManyField(User)
