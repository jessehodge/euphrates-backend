from rest_framework import serializers

from v1.data.job_tags.models import JobTags


class JobTagSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'name',
            'job_base_rate',
            'job_unit_rate',
            'unit',
            'status',
            'providers',
        )
        model = JobTags
