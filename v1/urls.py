from django.urls import include, re_path
from rest_framework.routers import DefaultRouter

from v1.data.chats import urls as chat_urls
from v1.data.devices import urls as device_urls
from v1.data.job_tags import urls as job_tag_urls
from v1.data.jobs import urls as job_urls
from v1.data.payments import urls as payment_urls
from v1.data.users import urls as user_urls

router = DefaultRouter()

urlpatterns = [
    re_path(r'^', include(user_urls)),
    re_path(r'^', include(job_tag_urls)),
    re_path(r'^', include(device_urls)),
    re_path(r'^', include(job_urls)),
    re_path(r'^', include(chat_urls)),
    re_path(r'^', include(payment_urls))
]